packer {
  required_plugins {
    amazon = {
      version = ">= 1.2.8"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

variable "arch" {
  type = string
}

variable "manifest_file" {
  type = string
}

variable "gitlab_pipeline_id" {
  type    = number
  default = 0
}

variable "gitlab_pipeline_url" {
  type    = string
  default = ""
}

variable "gitlab_job_id" {
  type    = number
  default = 0
}

variable "gitlab_job_url" {
  type    = string
  default = ""
}

variable "gitlab_commit_ref_name" {
  type    = string
  default = ""
}

variable "gitlab_commit_sha" {
  type    = string
  default = ""
}

locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
  instance_types = {
    amd64 = "t2.micro"
    arm64 = "t4g.micro"
  }
}

source "amazon-ebs" "ubuntu" {
  ami_name        = "linux-${var.arch}-ephemeral-${local.timestamp}"
  ami_description = "Ubuntu 22.04 AMI with Docker installed"
  instance_type   = local.instance_types[var.arch]
  region          = "us-east-1"
  ssh_username    = "ubuntu"

  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*ubuntu-jammy-22.04-${var.arch}-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }

  tags = {
    OS        = "Ubuntu"
    Version   = "22.04"
    CreatedAt = local.timestamp

    GitlabPipelineID    = var.gitlab_pipeline_id
    GitlabPipelineURL   = var.gitlab_pipeline_url
    GitlabJobID         = var.gitlab_job_id
    GitlabJobURL        = var.gitlab_job_url
    GitlabCommitRefName = var.gitlab_commit_ref_name
    GitlabCommitSHA     = var.gitlab_commit_sha
  }
}

build {
  name = "linux-${var.arch}-ephemeral"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]

  provisioner "shell" {
    execute_command   = "echo 'vagrant' | {{.Vars}} sudo -S -E bash -eux '{{.Path}}'"
    expect_disconnect = true

    scripts = [
      "./scripts/linux/01_install_docker.sh",
      "./scripts/linux/99_cleanup.sh"
    ]

    env = {
      DEBIAN_FRONTEND = "noninteractive"
    }
  }

  post-processor "manifest" {
    output     = var.manifest_file
    strip_path = true
  }
}
