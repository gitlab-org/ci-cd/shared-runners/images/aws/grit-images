package main

import (
	"log"
	"os"

	"gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/cmd/delete"
	"gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/cmd/drift"
	"gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/cmd/manifest"

	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Name: "grit-images",
		Commands: []*cli.Command{
			manifest.MergeManifestCMD,
			drift.DriftDetectionCmd,
			delete.DeletePipelineAMIsCMD,
			delete.DeleteAMIsFromAMIListFileCMD,
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
