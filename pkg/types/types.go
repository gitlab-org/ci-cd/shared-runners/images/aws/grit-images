package types

type GritVersion string

type MergedManifests struct {
	Manifests map[GritVersion]Manifest `json:"manifests"`
}

type UseCase string
type Region string

type Manifest map[UseCase]map[Region]AMI

type AMI struct {
	Id              string `json:"id,"`
	Name            string `json:"name"`
	Region          Region `json:"region"`
	OS              string `json:"os"`
	Arch            string `json:"arch"`
	Role            string `json:"role"`
	CreationDate    string `json:"creation_date"`
	DeprecationTime string `json:"deprecation_time"`
	Public          bool   `json:"public"`

	// used only for processing drift, is not included in manifests
	GritVersion GritVersion `json:"grit_version,omitempty"`
}

type AMIListFile struct {
	AMIs []AMI `json:"amis"`
}

type BuildsFile struct {
	Builds      []Build `json:"builds"`
	LastRunUuid string  `json:"last_run_uuid"`
}

type Build struct {
	Name          string      `json:"name"`
	BuilderType   string      `json:"builder_type"`
	BuildTime     int         `json:"build_time"`
	Files         interface{} `json:"files"`
	ArtifactId    string      `json:"artifact_id"`
	PackerRunUuid string      `json:"packer_run_uuid"`
	CustomData    interface{} `json:"custom_data"`
}
