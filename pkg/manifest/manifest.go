package manifest

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/pkg/types"

	"github.com/urfave/cli/v2"
)

const (
	PathArg = "manifest-path"

	DefaultPath = "./grit/modules/aws/ami_lookup/manifest.json"
)

func CombineJSON(ctx *cli.Context) error {
	manifestPath := ctx.String(PathArg)

	manifest := types.Manifest{}

	publishedDir := "published/"
	regionFiles, err := os.ReadDir(publishedDir)
	if err != nil {
		return fmt.Errorf("error reading use case directory: %v", err)
	}

	for _, fileEntry := range regionFiles {
		if filepath.Ext(fileEntry.Name()) == ".json" {
			filePath := filepath.Join(publishedDir, fileEntry.Name())
			fileData, err := os.ReadFile(filePath)
			if err != nil {
				return fmt.Errorf("error reading region file: %v", err)
			}

			var ami types.AMI
			err = json.Unmarshal(fileData, &ami)
			if err != nil {
				return fmt.Errorf("error unmarshaling JSON: %v", err)
			}

			useCase := types.UseCase(fmt.Sprintf("%v-%v-%v", ami.OS, ami.Arch, ami.Role))
			if _, ok := manifest[useCase]; !ok {
				manifest[useCase] = make(map[types.Region]types.AMI, 0)
			}

			manifest[useCase][ami.Region] = ami
		}
	}

	// format the JSON
	manifestBytes, err := json.MarshalIndent(manifest, "", "  ")
	if err != nil {
		return fmt.Errorf("error marshaling final JSON: %v", err)
	}

	// Extract the directory from the manifest path
	dir := filepath.Dir(manifestPath)

	// Create the directory and all necessary parents
	err = os.MkdirAll(dir, 0755)
	if err != nil {
		return fmt.Errorf("error creating directories: %v", err)
	}

	// Write the output to manifest.json
	err = os.WriteFile(manifestPath, manifestBytes, 0644)
	if err != nil {
		return fmt.Errorf("error writing manifest.json: %v", err)
	}

	// Print the contents of manifest.json
	fmt.Println(string(manifestBytes))

	return nil
}
