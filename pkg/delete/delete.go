package delete

import (
	"context"
	"fmt"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
)

func DeleteAMIAndSnapshot(svc *ec2.Client, amiID string) error {
	// Describe the AMI to find the associated snapshots
	describeImagesInput := &ec2.DescribeImagesInput{
		ImageIds: []string{amiID},
	}
	describeImagesOutput, err := svc.DescribeImages(context.Background(), describeImagesInput)
	if err != nil {
		return fmt.Errorf("failed to describe AMI %s: %w", amiID, err)
	}

	var snapshotIDs []string
	for _, image := range describeImagesOutput.Images {
		for _, blockDeviceMapping := range image.BlockDeviceMappings {
			if blockDeviceMapping.Ebs != nil {
				snapshotIDs = append(snapshotIDs, *blockDeviceMapping.Ebs.SnapshotId)
			}
		}
	}

	// Deregister the AMI
	deregisterImageInput := &ec2.DeregisterImageInput{
		ImageId: aws.String(amiID),
	}
	_, err = svc.DeregisterImage(context.Background(), deregisterImageInput)
	if err != nil {
		return fmt.Errorf("failed to deregister AMI %s: %w", amiID, err)
	}

	// Delete the associated snapshots
	for _, snapshotID := range snapshotIDs {
		deleteSnapshotInput := &ec2.DeleteSnapshotInput{
			SnapshotId: aws.String(snapshotID),
		}
		_, err = svc.DeleteSnapshot(context.Background(), deleteSnapshotInput)
		if err != nil {
			return fmt.Errorf("failed to delete snapshot %s: %v", snapshotID, err)
		} else {
			fmt.Println("successfully deleted snapshot", snapshotID)
		}
	}

	return nil
}
