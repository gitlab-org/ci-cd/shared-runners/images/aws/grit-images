package drift

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/pkg/types"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	ec2types "github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"github.com/urfave/cli/v2"
	gitlab "gitlab.com/gitlab-org/api/client-go"
)

const (
	UntaggedAMIsPathArg    = "untagged-amis-path"
	OldUntaggedAMIsPathArg = "old-untagged-amis-path"
	MissingAMIsPathArg     = "missing-amis-path"
	gritProjectId          = "48756626"
)

func DriftDetection(ctx *cli.Context) error {
	untaggedAMIsPath := ctx.String(UntaggedAMIsPathArg)
	oldUntaggedAMIsPath := ctx.String(OldUntaggedAMIsPathArg)
	missingAMIsPath := ctx.String(MissingAMIsPathArg)

	allTaggedAMIs, err := getTaggedManifestsAMIs()
	if err != nil {
		return fmt.Errorf("error getting tagged manifests: %v", err)
	}

	missingAmis, untaggedAmis, err := checkAMIsInAWS(allTaggedAMIs)
	if err != nil {
		return fmt.Errorf("error checking AMIs in AWS: %v", err)
	}

	latestReleaseDate, err := GetLatestReleaseDate(gritProjectId)
	if err != nil {
		return err
	}

	oldUntaggedAmis := make([]types.AMI, 0)
	for i := range untaggedAmis {
		cd, err := time.Parse(time.RFC3339, untaggedAmis[i].CreationDate)
		if err != nil {
			return err
		}
		if cd.Before(*latestReleaseDate) {
			oldUntaggedAmis = append(oldUntaggedAmis, untaggedAmis[i])
		}
	}

	fmt.Printf("\n\n%v MISSING AMIS (USED BY TAGGED GRIT VERSIONS AND MISSING IN AWS):\n", len(missingAmis))
	err = outputAndSaveResults(missingAMIsPath, missingAmis)

	fmt.Printf("\n\n%v UNTAGGED AMIS (UNUSED BY TAGGED GRIT VERSIONS):\n", len(untaggedAmis))
	err = outputAndSaveResults(untaggedAMIsPath, untaggedAmis)

	fmt.Printf("\n\n%v UNTAGGED AMIS OLDER THAN MOST RECENT TAGGED COMMIT (ELIGIBLE FOR DELETION):\n", len(untaggedAmis))
	err = outputAndSaveResults(oldUntaggedAMIsPath, oldUntaggedAmis)

	if err != nil {
		return fmt.Errorf("error outputting drift results: %v", err)
	}

	if len(missingAmis) > 0 {
		return fmt.Errorf("%v manifest AMIs from tagged releases are missing", len(missingAmis))
	}

	return nil
}

func getTaggedManifestsAMIs() ([]types.AMI, error) {
	repoURL := "https://gitlab.com/gitlab-org/ci-cd/runner-tools/grit.git"
	tmpDir, err := os.MkdirTemp("", "repo")
	if err != nil {
		return nil, fmt.Errorf("error creating temp dir: %v", err)
	}
	defer os.RemoveAll(tmpDir)

	// Clone the repository
	if err := exec.Command("git", "clone", repoURL, tmpDir).Run(); err != nil {
		return nil, fmt.Errorf("error cloning repository: %v", err)
	}

	// Get all tags
	tagsCmd := exec.Command("git", "-C", tmpDir, "tag")
	tagsOutput, err := tagsCmd.Output()
	if err != nil {
		return nil, fmt.Errorf("error retrieving tags: %v", err)
	}

	tags := strings.Split(string(tagsOutput), "\n")

	allTaggedAMIs := make([]types.AMI, 0)

	for _, tag := range tags {
		if tag == "" {
			continue
		}

		// Checking out each tag
		if err := exec.Command("git", "-C", tmpDir, "checkout", tag).Run(); err != nil {
			return nil, fmt.Errorf("error checking out tag: %v: %v", tag, err)
		}

		// Attempt to read the manifest.json file
		manifestPath := filepath.Join(tmpDir, "modules", "aws", "ami_lookup", "manifest.json")
		file, err := os.ReadFile(manifestPath)
		if err != nil {
			fmt.Println(fmt.Errorf("manifest.json not found for tag: %v: %v", tag, err))
			continue
		}

		tagManifest := types.Manifest{}
		if err := json.Unmarshal(file, &tagManifest); err != nil {
			return nil, fmt.Errorf("error unmarshalling JSON for tag: %v: %v", tag, err)
		}

		for _, uc := range tagManifest {
			for _, ami := range uc {
				ami.GritVersion = types.GritVersion(tag)
				allTaggedAMIs = append(allTaggedAMIs, ami)
			}
		}
	}

	// At this point, mergedManifests contains all items from all manifest.json files
	// Convert it back to JSON to see the result
	mergedJSON, err := json.MarshalIndent(allTaggedAMIs, "", "  ")
	if err != nil {
		return nil, fmt.Errorf("error marshalling merged manifest: %v", err)
	}
	fmt.Println("\nALL AMIS FROM ALL MANIFESTS:")
	fmt.Println(string(mergedJSON))

	return allTaggedAMIs, nil
}

func GetLatestReleaseDate(projectID string) (*time.Time, error) {
	client, err := gitlab.NewClient("", nil)
	if err != nil {
		return nil, err
	}

	latestRelease, r, err := client.Releases.GetLatestRelease(projectID)
	if err != nil {
		return nil, fmt.Errorf("failed to get latest release: %v: %v", r, err)
	}

	return latestRelease.CreatedAt, nil
}

func checkAMIsInAWS(allTaggedImages []types.AMI) ([]types.AMI, []types.AMI, error) {
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		return nil, nil, fmt.Errorf("error loading AWS SDK config: %v", err)
	}

	// Create an EC2 client to list all regions
	ec2Client := ec2.NewFromConfig(cfg)
	regionsResult, err := ec2Client.DescribeRegions(context.TODO(), &ec2.DescribeRegionsInput{})
	if err != nil {
		return nil, nil, fmt.Errorf("error describing regions: %v", err)
	}

	allImagesInAws := make([]types.AMI, 0)
	for _, region := range regionsResult.Regions {
		images, err := getImagesFromRegion(region)
		if err != nil {
			return nil, nil, fmt.Errorf("error describing images: %v", err)
		}
		allImagesInAws = append(allImagesInAws, images...)
	}

	// Check if AMIs from manifest are missing from AWS
	amiSet := make(map[string]struct{})
	for _, image := range allImagesInAws {
		amiSet[image.Id] = struct{}{}
	}
	missingAMIs := make([]types.AMI, 0)
	for _, ami := range allTaggedImages {
		if _, exists := amiSet[ami.Id]; !exists {
			missingAMIs = append(missingAMIs, ami)
		}
	}

	// Check which images in AWS are not needed by any tagged manifests
	mergedMap := make(map[string]struct{})
	for _, image := range allTaggedImages {
		mergedMap[image.Id] = struct{}{}
	}
	untaggedAMIs := make([]types.AMI, 0)
	for _, ami := range allImagesInAws {
		if _, exists := mergedMap[ami.Id]; !exists {
			untaggedAMIs = append(untaggedAMIs, ami)
		}
	}

	return missingAMIs, untaggedAMIs, nil
}

func getImagesFromRegion(region ec2types.Region) ([]types.AMI, error) {
	fmt.Println("CHECKING REGION: ", *region.RegionName)

	regionImages := make([]types.AMI, 0)

	// Update the config with the region for each iteration
	regionalCfg, err := config.LoadDefaultConfig(context.TODO(), config.WithRegion(*region.RegionName))
	if err != nil {
		return nil, fmt.Errorf("unable to load regional AWS SDK config: %v", err)
	}

	regionalEc2Client := ec2.NewFromConfig(regionalCfg)

	// Retrieve all AMIs in the current region
	for {
		amiResult, err := regionalEc2Client.DescribeImages(context.TODO(), &ec2.DescribeImagesInput{
			Owners: []string{"self"},
		})
		if err != nil {
			return nil, fmt.Errorf("unable to describe AMIs in region %s: %v", *region.RegionName, err)
		}

		for _, image := range amiResult.Images {
			a, err := awsImageToAMIStruct(region, image)
			if err != nil {
				return nil, fmt.Errorf("unable to convert AWS Image to AMI struct %s: %v", *region.RegionName, err)
			}
			regionImages = append(regionImages, a)
		}

		if amiResult.NextToken == nil || len(amiResult.Images) == 0 {
			break
		}
	}

	return regionImages, nil
}

func awsImageToAMIStruct(region ec2types.Region, image ec2types.Image) (types.AMI, error) {
	if image.ImageId == nil {
		return types.AMI{}, fmt.Errorf("image does not contain an ImageId pointer: %#v", image)
	}
	if image.Public == nil {
		return types.AMI{}, fmt.Errorf("image does not contain a Public pointer: %#v", image)
	}

	arch, err := getImageArch(image)
	if err != nil {
		return types.AMI{}, err
	}
	a := types.AMI{
		Id:     *image.ImageId,
		Region: types.Region(*region.RegionName),
		Arch:   arch,
		Public: *image.Public,
	}

	if image.Name != nil {
		a.Name = *image.Name
	}
	if image.DeprecationTime != nil {
		a.DeprecationTime = *image.DeprecationTime
	}
	if image.CreationDate != nil {
		a.CreationDate = *image.CreationDate
	}

	return a, nil
}

func getImageArch(image ec2types.Image) (string, error) {
	arch, ok := map[string]string{
		"x86_64":    "amd64",
		"arm64":     "arm64",
		"arm64_mac": "arm64",
	}[string(image.Architecture)]
	if !ok {
		return "", fmt.Errorf("No arch found for %v", image.Architecture)
	}
	return arch, nil
}

func outputAndSaveResults(amisListFilePath string, amis []types.AMI) error {
	amiListFile := types.AMIListFile{
		AMIs: amis,
	}
	amisJSON, err := json.MarshalIndent(amiListFile, "", "  ")
	if err != nil {
		return fmt.Errorf("error marshalling untagged amis: %v", err)
	}
	fmt.Println(string(amisJSON))

	// Extract the directory from the path
	dir := filepath.Dir(amisListFilePath)

	// Create the directory and all necessary parents
	err = os.MkdirAll(dir, 0755)
	if err != nil {
		return fmt.Errorf("error creating directories: %v", err)
	}

	// Write the output to json
	err = os.WriteFile(amisListFilePath, amisJSON, 0644)
	if err != nil {
		return fmt.Errorf("error writing %v: %v", amisListFilePath, err)
	}

	return nil
}
