package drift

import (
	"gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/pkg/drift"

	"github.com/urfave/cli/v2"
)

var DriftDetectionCmd = &cli.Command{
	Name:   "drift",
	Usage:  "look for AMIs that are not available or extras that can be deleted",
	Action: drift.DriftDetection,
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:     drift.UntaggedAMIsPathArg,
			Required: true,
			Usage:    "Path at which the list of untagged amis should be saved",
		},
		&cli.StringFlag{
			Name:     drift.OldUntaggedAMIsPathArg,
			Required: true,
			Usage:    "Path at which the list of old untagged amis should be saved. These are AMIs that were created before the commit of the most recent release of GRIT.",
		},
		&cli.StringFlag{
			Name:     drift.MissingAMIsPathArg,
			Required: true,
			Usage:    "Path at which the list of missing amis should be saved",
		},
	},
}
