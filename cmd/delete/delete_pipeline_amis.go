package delete

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/pkg/aws_util"
	"gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/pkg/delete"
	"gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/pkg/types"

	"github.com/urfave/cli/v2"
)

var DeletePipelineAMIsCMD = &cli.Command{
	Name:   "delete-pipeline-amis",
	Usage:  "delete all AMIs created in the current pipeline and recorded in the builds/ directory",
	Action: DeletePipelineAMIs,
}

func DeletePipelineAMIs(_ *cli.Context) error {
	svc, err := aws_util.NewClient("us-east-1")

	files, err := getBuildFiles("builds/")
	if err != nil {
		return fmt.Errorf("failed to get build files: %v", err)
	}

	for _, file := range files {
		buildFile, err := parseBuildFile(file)
		if err != nil {
			return fmt.Errorf("failed to parse file %s: %v", file, err)
		}

		// Delete the AMI and its associated snapshot
		amiID := strings.Split(buildFile.Builds[0].ArtifactId, ":")[1]
		err = delete.DeleteAMIAndSnapshot(svc, amiID)
		if err != nil {
			return fmt.Errorf("failed to delete AMI %s and its snapshot: %v", amiID, err)
		} else {
			fmt.Println("successfully deleted AMI", amiID, "and its snapshot")
		}
	}

	return nil
}

func getBuildFiles(directory string) ([]string, error) {
	var files []string
	err := filepath.Walk(directory, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && filepath.Ext(path) == ".json" {
			files = append(files, path)
		}
		return nil
	})
	return files, err
}

func parseBuildFile(path string) (types.BuildsFile, error) {
	var buildFile types.BuildsFile
	data, err := os.ReadFile(path)
	if err != nil {
		return buildFile, err
	}
	err = json.Unmarshal(data, &buildFile)
	return buildFile, err
}
