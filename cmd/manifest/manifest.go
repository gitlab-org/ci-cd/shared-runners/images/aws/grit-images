package manifest

import (
	"gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/pkg/manifest"

	"github.com/urfave/cli/v2"
)

var MergeManifestCMD = &cli.Command{
	Name:   "manifest",
	Usage:  "merge all published ami json files into a single manifest",
	Action: manifest.CombineJSON,
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:  manifest.PathArg,
			Usage: "Path at which the manifest file should be saved",
			Value: manifest.DefaultPath,
		},
	},
}
