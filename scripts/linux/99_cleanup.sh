#!/bin/sh -eux

# Sources for delete from the following two sources:
# Cleanup script from hashicorp packer examples:
# https://github.com/hashicorp/packer/blob/407693404b0c278e3c3fde0ce6258babb9d2040e/examples/hcl/linux/etc/scripts/100-cleanup.sh
# Guidelines for shared linux AMIs operations:
# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/building-shared-amis.html

# Delete all Linux headers
sudo dpkg --list \
  | awk '{ print $2 }' \
  | grep 'linux-headers' \
  | xargs apt-get -y purge;

# Remove specific Linux kernels, such as linux-image-3.11.0-15-generic but
# keeps the current kernel and does not touch the virtual packages,
# e.g. 'linux-image-generic', etc.
sudo dpkg --list \
    | awk '{ print $2 }' \
    | grep 'linux-image-.*-generic' \
    | grep -v "$(uname -r)" \
    | xargs apt-get -y purge;

# Delete Linux source
sudo dpkg --list \
    | awk '{ print $2 }' \
    | grep linux-source \
    | xargs apt-get -y purge;

# Delete development packages
sudo dpkg --list \
    | awk '{ print $2 }' \
    | grep -- '-dev$' \
    | xargs apt-get -y purge;

# delete docs packages
sudo dpkg --list \
    | awk '{ print $2 }' \
    | grep -- '-doc$' \
    | xargs apt-get -y purge;

# Delete X11 libraries
apt-get -y purge libx11-data xauth libxmuu1 libxcb1 libx11-6 libxext6;

# 19.10+ don't have this package so fail gracefully
apt-get -y purge command-not-found-data || true;

# Exlude the files we don't need w/o uninstalling linux-firmware
echo "==> Setup dpkg excludes for linux-firmware"
cat <<_EOF_ | cat >> /etc/dpkg/dpkg.cfg.d/excludes
#BENTO-BEGIN
path-exclude=/lib/firmware/*
path-exclude=/usr/share/doc/linux-firmware/*
#BENTO-END
_EOF_

# Delete the massive firmware packages
rm -rf /lib/firmware/*
rm -rf /usr/share/doc/linux-firmware/*

apt-get -y autoremove;
apt-get -y clean;

# Remove docs
rm -rf /usr/share/doc/*

# Remove caches
find /var/cache -type f -exec rm -rf {} \;

# truncate any logs that have built up during the install
find /var/log -type f -exec truncate --size=0 {} \;

# Blank netplan machine-id (DUID) so machines get unique ID generated on boot.
truncate -s 0 /etc/machine-id

# remove the contents of /tmp and /var/tmp
rm -rf /tmp/* /var/tmp/*

# clear the history so our install isn't there
export HISTSIZE=0
rm -f /root/.wget-hsts

############################################################
# Beginning of Guidelines for shared linux AMIs operations #
############################################################

# Disable password-based remote logins for the root user
sudo sed -i 's/#PermitRootLogin yes/PermitRootLogin without-password/' /etc/ssh/sshd_config

# Disable direct root logins
sudo passwd -l root

# Remove SSH host key pairs
sudo shred -u /etc/ssh/*_key /etc/ssh/*_key.pub
