#!/bin/bash

set -euo pipefail
set -x

# Install docker
echo "Installing docker"

sudo -E apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y curl
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker "$USER"
sudo usermod -aG docker ubuntu
sudo service docker start
sudo apt-get clean
sudo rm -rf /var/lib/apt/lists/*
