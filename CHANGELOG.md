## v0.1.0 (2025-03-05)

### New features

- Chore: Add changelog configuration [!35](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/-/merge_requests/35)
- chore: Remove branch name check in prerelease tagging [!40](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/-/merge_requests/40)
- chore: Fix prerelease tag job dependencies [!39](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/-/merge_requests/39)
- chore: Fix release URL and documentation links for GRIT Images project [!37](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/-/merge_requests/37)
- Add automated prerelease job and make target [!36](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/-/merge_requests/36)
- Add stable release job [!33](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/-/merge_requests/33)
- chore: Add release automation configuration [!38](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/-/merge_requests/38)
- Add versioning tooling [!34](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/-/merge_requests/34)

### Maintenance

- Chore: move print statement so it isn't repeated for every region [!32](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/-/merge_requests/32)

