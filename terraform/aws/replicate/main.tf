resource "aws_ami_copy" "new_ami" {
  name              = var.source_ami_name
  description       = "A copy of ${var.source_ami_id}"
  source_ami_id     = var.source_ami_id
  source_ami_region = "us-east-1"

  tags = {
    Name = var.source_ami_name
  }
}

resource "local_file" "new_ami" {
  content = jsonencode({
    name             = aws_ami_copy.new_ami.name
    id               = aws_ami_copy.new_ami.id
    public           = aws_ami_copy.new_ami.public
    os               = var.os
    arch             = var.arch
    role             = var.role
    deprecation_time = aws_ami_copy.new_ami.deprecation_time
  })
  filename = "./copies/aws-${var.os}-${var.arch}-${var.role}-${var.target_region}.json"
}

resource "aws_ami_launch_permission" "grit-account" {
  image_id = aws_ami_copy.new_ami.id
  # 767397899482 is the GRIT production account
  account_id = "767397899482"
}
