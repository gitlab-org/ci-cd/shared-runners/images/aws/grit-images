variable "source_ami_id" {
  description = "The AMI to copy"
  type        = string
}

variable "source_ami_name" {
  description = "The name of the AMI to copy"
  type        = string
}

variable "os" {
  description = "The AMI operating system (linux)"
  type        = string
}

variable "arch" {
  description = "The AMI architecture (amd64 or arm64)"
  type        = string
}

variable "role" {
  description = "The role of the AMI (ephermeral)"
  type        = string
}

variable "target_region" {
  description = "The region of the new AMI copy"
  type        = string
}
