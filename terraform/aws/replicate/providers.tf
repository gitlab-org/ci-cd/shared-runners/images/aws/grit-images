provider "aws" {
  region = var.target_region
}

provider "aws" {
  alias  = "source"
  region = "us-east-1"
}