resource "aws_servicequotas_service_quota" "public_ami_quota" {
  service_code = "ec2"
  # Public AMI quota code, e.g open
  # https://us-east-1.console.aws.amazon.com/servicequotas/home/services/ec2/quotas/L-0E3CBAB9
  quota_code = "L-0E3CBAB9"
  value      = var.public_ami_quota
}

resource "aws_ami_launch_permission" "public" {
  image_id = var.ami_id
  group    = "all"

  # Quota increase might be async, especially if the delta is large.
  # Depend on the quota in case it is immediate.
  depends_on = [
    aws_servicequotas_service_quota.public_ami_quota
  ]
}

data "aws_ami" "ami" {
  owners = null

  filter {
    name   = "image-id"
    values = [var.ami_id]
  }

  // Make sure we've updated permissions before we pull latest data on AMI
  depends_on = [
    aws_ami_launch_permission.public
  ]
}

resource "local_file" "new_ami" {
  content = jsonencode({
    name             = data.aws_ami.ami.name
    id               = data.aws_ami.ami.id
    public           = data.aws_ami.ami.public
    os               = var.os
    arch             = var.arch
    role             = var.role
    deprecation_time = data.aws_ami.ami.deprecation_time
    region           = var.target_region
  })
  filename = "${path.module}/published/aws-${var.os}-${var.arch}-${var.role}-${var.target_region}.json"
}
