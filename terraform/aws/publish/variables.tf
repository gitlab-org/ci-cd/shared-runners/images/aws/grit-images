variable "ami_id" {
  description = "The AMI to publish"
  type        = string
}

variable "target_region" {
  description = "The region of the new AMI copy"
  type        = string
}

variable "os" {
  description = "The AMI operating system"
  type        = string
}

variable "arch" {
  description = "The AMI architecture"
  type        = string
}

variable "role" {
  description = "The role of the AMI"
  type        = string
}

variable "public_ami_quota" {
  description = "The desired Public AMI quota for the target region"
  type        = number
}
