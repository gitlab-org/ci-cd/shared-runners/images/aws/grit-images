export VERSION := $(shell ./ci/version)
REVISION := $(shell git rev-parse --short=8 HEAD || echo unknown)
BRANCH := $(shell git show-ref | grep "$(REVISION)" | grep -v HEAD | awk '{print $$2}' | sed 's|refs/remotes/origin/||' | sed 's|refs/heads/||' | sort | head -n 1)
LATEST_STABLE_TAG := $(shell git -c versionsort.prereleaseSuffix="-pre" tag -l "v*.*.*" | sort -rV | awk '!/rc/' | head -n 1)

.PHONY: version
version:
	@echo Current version: $(VERSION)
	@echo Current revision: $(REVISION)
	@echo Current branch: $(BRANCH)
	@echo Latest stable: $(LATEST_STABLE_TAG)

.PHONY: prerelease-tag
prerelease-tag:
	@echo "Checking version consistency..."
	@file_version=v$$(cat VERSION) && \
	repo_version=$$(git describe --abbrev=0 --tags --exclude='*-pre*' 2>/dev/null || echo "0.0.0") && \
	if [ "$$file_version" != "$$repo_version" ]; then \
		echo "Error: Repository version ($$repo_version) doesn't match VERSION file ($$file_version)"; \
		exit 1; \
	fi
	@git fetch origin main && \
	local_commit=$$(git rev-parse HEAD) && \
	remote_commit=$$(git rev-parse origin/main) && \
	if [ "$$local_commit" != "$$remote_commit" ]; then \
		echo "Error: Local main ($$local_commit) is not in sync with remote main ($$remote_commit)"; \
		exit 1; \
	fi
	@echo "Configuring git user..."
	git config --global user.email "auto-runner-releaser@gitlab.com"
	git config --global user.name "Auto Runner Releaser"
	@export PRERELEASE_TAG=$$(./ci/version) && \
	echo "Creating tag: $$PRERELEASE_TAG" && \
	git tag -a $$PRERELEASE_TAG -m "Prerelease $$PRERELEASE_TAG" && \
	git push https://oauth2:$$GRIT_IMAGES_RELEASE_GITLAB_TOKEN@gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images.git $$PRERELEASE_TAG
