GITLAB_PROJECT_ID ?= 54252315
TF_PROJECT_ID ?= 39258790
NAME ?= grit-e2e
RUNNER_TAG ?= default
AMI_ID ?= $(shell jq -r '.builds[-1].artifact_id | split(":")[1]' "$(MANIFEST_FILE)")

.PHONY: e2e-terraform-apply
e2e-terraform-apply: .check_credentials .check_region e2e-download-grit e2e-terraform-init
e2e-terraform-apply:
	# Applying Terraform state
	@terraform -chdir=terraform/aws/e2e apply \
		-auto-approve \
		-var=os=$(OS) \
		-var=arch=$(ARCH) \
		-var=ami=$(AMI_ID) \
		-var=name=$(NAME) \
		-var=gitlab_project_id=$(GITLAB_PROJECT_ID) \
		-var=runner_tag=$(RUNNER_TAG)

.PHONY: e2e-terraform-destroy
e2e-terraform-destroy: .check_credentials .check_region e2e-download-grit e2e-terraform-init
e2e-terraform-destroy:
	# Destroying Terraform state
	@terraform -chdir=terraform/aws/e2e destroy \
		-auto-approve \
		-var=os=$(OS) \
		-var=arch=$(ARCH) \
		-var=ami=$(AMI_ID) \
		-var=name=$(NAME) \
		-var=gitlab_project_id=$(GITLAB_PROJECT_ID) \
		-var=runner_tag=$(RUNNER_TAG)
	# Deleting Terraform state
	@curl -s -w '\nstatus code: %{http_code}\n' \
		--header "Private-Token: $(TF_HTTP_PASSWORD)" \
		--request DELETE "${TF_HTTP_ADDRESS}"
	# Done!

.PHONY: e2e-download-grit
e2e-download-grit: GRIT_VERSION = v0.8.0
e2e-download-grit: DOWNLOAD_URL = https://gitlab.com/gitlab-org/ci-cd/runner-tools/grit/-/archive/$(GRIT_VERSION)/grit-$(GRIT_VERSION).zip
e2e-download-grit: LOCAL = $(PWD)/.local
e2e-download-grit:
	# Downloading grit from $(DOWNLOAD_URL) to $(LOCAL)/grit
	@mkdir -p "$(LOCAL)"
	@curl -sL "$(DOWNLOAD_URL)" -o "$(LOCAL)/grit.zip"
	@unzip -q "$(LOCAL)/grit.zip" -d "$(LOCAL)/"
	@rm -rf "$(LOCAL)/grit"
	@mv -f "$(LOCAL)/grit-$(GRIT_VERSION)" "$(LOCAL)/grit"
	@rm "$(LOCAL)/grit.zip"

.PHONY: e2e-terraform-init
e2e-terraform-init: .check_credentials .check_region e2e-download-grit
e2e-terraform-init:
	# Initializing Terraform state
	@terraform -chdir=terraform/aws/e2e init

.PHONY: e2e-terraform-clean
e2e-terraform-clean:
	# Removing local Terraform state
	rm -rf terraform/aws/e2e/.terraform*
