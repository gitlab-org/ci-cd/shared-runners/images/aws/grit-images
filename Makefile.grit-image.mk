# Credentials should be set externally!
AWS_DEFAULT_REGION ?= unknown
AWS_ACCESS_KEY_ID ?= unknown
AWS_SECRET_ACCESS_KEY ?= unknown

#
# Images are currently built in our Sandbox account
# A manual job will publish them to other accounts like the one
# where we host our staging or production environments
#
# 767397899482 is the GRIT production account
#
PUBLISH_ACCOUNT_ID ?= 767397899482

# BUILD_ENVIRONMENT defines where the image will be built.
# Local execution will use the verify-runner sandbox account,
# while the CI execution will use the SaaS MacOS Staging runners
# account.
BUILD_ENVIRONMENT ?= sandbox

# PROVIDER defines which cloud provider should be built
PROVIDER ?= unknown

# OS defines the operating system of the image
OS ?= unknown

# ARCH defines the architecture of the image
ARCH ?= unknown

# ROLE defines the role of the image
ROLE ?= ephemeral

# IMAGE_TEMPLATE defines which image configuration should be built
IMAGE_TEMPLATE ?= $(PROVIDER)-$(OS)-$(ROLE)

# SOURCE_AMI_ID defines which id of the AMI that was built for the current use case being processed
SOURCE_AMI_ID ?= unknown

# SOURCE_AMI_NAME defines which name of the image configuration that was built for the current use case being processed
SOURCE_AMI_NAME ?= unknown

# TARGET_REGION defines which region to copy an AMI to
TARGET_REGION ?= unknown

# MANIFEST_FILE contains the machine usable output of `packer build`
# execution. It can be used to read the ID of the built AMI.
MANIFEST_FILE ?= builds/$(PROVIDER)-$(OS)-$(ARCH)-$(ROLE).json

# PUBLIC_AMI_QUOTA defines the desired Public AMI quota for the target region
# 5 is the default value
PUBLIC_AMI_QUOTA ?= 5

.PHONY: packer-init
packer-init:
	packer init packer/templates/$(IMAGE_TEMPLATE).pkr.hcl

.PHONY: packer-validate
packer-validate: packer-init
	mkdir -p "builds"
	packer validate \
		-var "arch=$(ARCH)" \
		-var "manifest_file=$(MANIFEST_FILE)" \
		packer/templates/$(IMAGE_TEMPLATE).pkr.hcl

.PHONY: packer-fmt
packer-fmt: packer-init
	packer fmt -recursive packer/
	git --no-pager diff --compact-summary --exit-code -- packer/

.PHONY: packer-build
packer-build: packer-validate .check_credentials
	packer build \
		-timestamp-ui \
		-var "arch=$(ARCH)" \
		-var "manifest_file=$(MANIFEST_FILE)" \
		-var "gitlab_pipeline_id=$(CI_PIPELINE_ID)" \
		-var "gitlab_pipeline_url=$(CI_PIPELINE_URL)" \
		-var "gitlab_job_id=$(CI_JOB_ID)" \
		-var "gitlab_job_url=$(CI_JOB_URL)" \
		-var "gitlab_commit_ref_name=$(CI_COMMIT_REF_NAME)" \
		-var "gitlab_commit_sha=$(CI_COMMIT_SHA)" \
		packer/templates/$(IMAGE_TEMPLATE).pkr.hcl
	@echo "========================================================================"
	@echo " Created image with AMI ID: $$(jq -r '.builds[-1].artifact_id | split(":")[1]' "$(MANIFEST_FILE)")"
	@echo "========================================================================"

.PHONY: packer-publish
packer-publish: AMI_ID ?= $(shell jq -r '.builds[-1].artifact_id | split(":")[1]' "$(MANIFEST_FILE)")
packer-publish: .check_credentials .check_region
	aws --output json \
		ec2 modify-image-attribute \
		--image-id "$(AMI_ID)" \
		--launch-permission "Add=[{UserId=$(PUBLISH_ACCOUNT_ID)}]" && \
	aws --output json \
		ec2 modify-snapshot-attribute \
		--snapshot-id $$(aws ec2 describe-images --image-ids "$(AMI_ID)" --query 'Images[0].BlockDeviceMappings[0].Ebs.SnapshotId' --output text) \
		--attribute createVolumePermission \
		--operation-type add \
		--user-ids $(PUBLISH_ACCOUNT_ID)

.PHONY: terraform-fmt
terraform-fmt:
	cd ./terraform && \
	terraform fmt -recursive
	git --no-pager diff --compact-summary --exit-code -- terraform/

.PHONY: replicate-aws
replicate-aws:
	$(eval SOURCE_AMI_ID := $(shell jq -r '.builds[-1].artifact_id | split(":")[1]' "$(MANIFEST_FILE)"))
	$(eval SOURCE_AMI_NAME := $(shell jq -r '.builds[-1].name' "$(MANIFEST_FILE)"))
	cd ./terraform/aws/replicate && \
	terraform init && \
	terraform apply \
		-var source_ami_name="$(SOURCE_AMI_NAME)" \
		-var source_ami_id=$(SOURCE_AMI_ID) \
		-var os=$(OS) \
		-var arch=$(ARCH) \
		-var role=$(ROLE) \
		-var target_region=$(TARGET_REGION) \
		-auto-approve
	mv ./terraform/aws/replicate/copies/ ./copies

.PHONY: publish-ami
publish-ami:
	$(eval MANIFEST_FILE := "copies/$(PROVIDER)-$(OS)-$(ARCH)-$(ROLE)-$(TARGET_REGION).json")
	$(eval AMI_ID := $(shell jq -r '.id' "$(MANIFEST_FILE)"))
	aws ec2 disable-image-block-public-access --region $(TARGET_REGION) && \
	cd ./terraform/aws/publish && \
	terraform init && \
	terraform apply \
		-var ami_id=$(AMI_ID) \
		-var os=$(OS) \
		-var arch=$(ARCH) \
		-var role=$(ROLE) \
		-var target_region=$(TARGET_REGION) \
		-var public_ami_quota=$(PUBLIC_AMI_QUOTA) \
		-auto-approve
	mv ./terraform/aws/publish/published/ ./published

.PHONY: grit-mr
grit-mr:
	@BRANCH_NAME="new-manifest-$$CI_PIPELINE_IID" && \
	git clone https://gitlab-ci-token:$$GRIT_PROJECT_ACCESS_TOKEN@gitlab.com/gitlab-org/ci-cd/runner-tools/grit.git && \
	go run . manifest
	cd grit && \
	git config --local user.email "na" && \
	git config --local user.name "dev token" && \
	git checkout -b $$BRANCH_NAME && \
	git add -A && \
	git commit -m "feat: New manifest auto-MR from grit-images job $$CI_JOB_ID" && \
	git push --set-upstream origin $$BRANCH_NAME -o merge_request.create -o merge_request.title="feat: New Manifest Version" -o merge_request.description="New manifest generated from https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/aws/grit-images/-/jobs/$$CI_JOB_ID" -o merge_request.assign="$$GITLAB_USER_LOGIN"

.PHONY: .check_credentials
.check_credentials:
ifeq ($(AWS_ACCESS_KEY_ID), unknown)
	@echo "AWS_ACCESS_KEY_ID has to be defined"
	@exit 1
endif
ifeq ($(AWS_SECRET_ACCESS_KEY), unknown)
	@echo "AWS_SECRET_ACCESS_KEY has to be defined"
	@exit 1
endif
	@echo "Credentials are set properly"

.PHONY: .check_region
.check_region:
ifeq ($(AWS_DEFAULT_REGION), unknown)
	@echo "AWS_DEFAULT_REGION has to be defined"
	@exit 1
endif
